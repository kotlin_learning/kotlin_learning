/*
 * 
 * Copyright (c) 2017, Multiva SA de CV
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are not permitted.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Developed 2017 by Legosoft www.legosoft.com.mx
 * 
 */

package com.local.kotlin.kotlinlegosoftlearning.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 
 * @author : Alfredo Cuevas Barrera
 * @email : acuevas@legosoft.com.mx
 * @project : multiva-seguridad-servicios-boot
 * @version : 1.0.0-SNAPSHOT
 * @class : DatabaseConfig.java
 * @since : 22/06/2017
 * @description : Clase que proporciona las configuraciones de la persistencia a
 *              base de datos
 *
 */

//@Configuration
//@EnableTransactionManagement
public class DatabaseConfig {

	// ###################3

	@Autowired
	private Environment env;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactory;

	/**
	 * 
	 * @nameMethod dataSource
	 * @return
	 * @description: El metodo dataSource tiene un tipo de retorno: DataSource,
	 *               el cual sirve para: configurar tu datasource desde el
	 *               archivo application.properties
	 *
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("db.driver"));
		dataSource.setUrl(env.getProperty("db.url"));
		dataSource.setUsername(env.getProperty("db.username"));
		dataSource.setPassword(env.getProperty("db.password"));
		return dataSource;
	}

	/**
	 * 
	 * @nameMethod entityManagerFactory
	 * @return
	 * @description: El metodo entityManagerFactory tiene un tipo de retorno:
	 *               LocalContainerEntityManagerFactoryBean, el cual sirve para:
	 *               Declarar la entidad manejadora de JPA.
	 *
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();

		entityManagerFactory.setDataSource(dataSource);

		// ESCANEO DE LOS @Component, @Service Y OTRAS CLASES ANOTADAS
		entityManagerFactory.setPackagesToScan(env.getProperty("entitymanager.packagesToScan"));

		// ADAPTADOR DEL VENDOR
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		entityManagerFactory.setJpaVendorAdapter(vendorAdapter);

		// PROPIEDADES HIBERNATE
		Properties additionalProperties = new Properties();
		additionalProperties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		additionalProperties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
		additionalProperties.put("hibernate.default_schema", env.getProperty("hibernate.default_schema"));
//		additionalProperties.put("hibernate.ejb.interceptor", env.getProperty("hibernate.ejb.interceptor"));

		entityManagerFactory.setJpaProperties(additionalProperties);

		return entityManagerFactory;
	}

	/**
	 * 
	 * @nameMethod transactionManager
	 * @return
	 * @description: El metodo transactionManager tiene un tipo de retorno:
	 *               JpaTransactionManager, el cual sirve para: Declarar el
	 *               manejo de transacciones.
	 *
	 */
	@Bean
	public JpaTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
		return transactionManager;
	}

	/**
	 * 
	 * @nameMethod exceptionTranslation
	 * @return
	 * @description: El metodo exceptionTranslation tiene un tipo de retorno:
	 *               PersistenceExceptionTranslationPostProcessor, el cual sirve
	 *               para: procesar los POST-Beans que agrega un advisor a
	 *               cualquier bean anotado para que cualquier excepcion
	 *               específica de plataforma sea capturada y luego vuelva a ser
	 *               utilizada como excepción de acceso a datos de Spring sin
	 *               marcar, es decir, una subclase de DataAccessException.
	 *
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

}
