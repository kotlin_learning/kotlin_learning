package com.local.kotlin.kotlinlegosoftlearning.service

import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona

interface ITipoPersonaService {

    fun consultaTiposPersonas(): MutableList<TipoPersona>?;
}
