package com.local.kotlin.kotlinlegosoftlearning

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.PropertySource

@SpringBootApplication
@PropertySource(value = "classpath:api.properties")
class KotlinLegosoftLearningApplication

fun main(args: Array<String>) {
//    runApplication<KotlinLegosoftLearningApplication>(*args)
    SpringApplication.run(KotlinLegosoftLearningApplication::class.java, *args)
}
