package com.local.kotlin.kotlinlegosoftlearning.model

import com.local.kotlin.kotlinlegosoftlearning.request.TipoPersonaRequest
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "TipoPersona")
class TipoPersona(
        @Field(value = "id")
        var id: Long) {
        @Field(value = "nombre")
    var nombre: String = "";

    constructor() : this(0)

    constructor(request: TipoPersonaRequest) : this() {
        this.id = request.id
        this.nombre = request.nombre
    }
}
