package com.local.kotlin.kotlinlegosoftlearning.response

class ErrorResponse {

    var responseStatus: Long? = 0
    var responseError: String? = ""
    var result: Any? = null
}