package com.local.kotlin.kotlinlegosoftlearning.dao.Impl

import com.local.kotlin.kotlinlegosoftlearning.dao.ITipoPersonaDaoEx
import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona
import org.springframework.stereotype.Repository

@Repository
class TipoPersonaDaoImplEx : GenericDaoImpl<TipoPersona, Long>(), ITipoPersonaDaoEx {
}
