package com.local.kotlin.kotlinlegosoftlearning.service.impl

import com.local.kotlin.kotlinlegosoftlearning.dao.IPersonaDao
import com.local.kotlin.kotlinlegosoftlearning.model.Persona
import com.local.kotlin.kotlinlegosoftlearning.request.PersonaRequest
import com.local.kotlin.kotlinlegosoftlearning.service.IPersonaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.Exception

@Service
class PersonaServiceImpl : IPersonaService {

    @Autowired
    lateinit var personaDao: IPersonaDao;

    @Throws(Exception::class)
    override fun creaPersona(request: PersonaRequest): Any? {
        try {
            return personaDao.savePersona(Persona(request))
        } catch (e: Exception) {
            throw Exception(e);
        }
    }

    override fun consultaPersonas(): Any? {
        try {
            return personaDao.consultaPersonas();
        } catch (e: Exception) {
            throw Exception(e);
        }
    }
}
