package com.local.kotlin.kotlinlegosoftlearning.controllers

import com.local.kotlin.kotlinlegosoftlearning.exception.CoreException
import com.local.kotlin.kotlinlegosoftlearning.model.Persona
import com.local.kotlin.kotlinlegosoftlearning.request.PersonaRequest
import com.local.kotlin.kotlinlegosoftlearning.response.ErrorResponse
import com.local.kotlin.kotlinlegosoftlearning.response.PersonaResponse
import com.local.kotlin.kotlinlegosoftlearning.service.IPersonaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.stream.Collectors
import javax.ws.rs.Consumes
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@CrossOrigin
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RestController
@RequestMapping("/persona/v1")
class PersonaRestService {

    @Autowired
    lateinit var personaService: IPersonaService;

    @PostMapping(value = "creaPersona")
    fun creaPersona(@RequestBody request: PersonaRequest): ResponseEntity<ErrorResponse> {

        var respuesta = ErrorResponse()
        var persona: Persona = personaService.creaPersona(request) as Persona
        try {
            respuesta.responseError = ""
            respuesta.responseStatus = 200
            respuesta.result = PersonaResponse(persona)
            return ResponseEntity(respuesta, HttpStatus.OK)
        } catch (e: CoreException) {
            respuesta.responseError = e.message
            respuesta.responseStatus = e.code
            respuesta.result = null
            return ResponseEntity(respuesta, HttpStatus.OK)
        }
    }

    @PostMapping(value = "consultaPersonas")
    fun consultaPersonas(): ResponseEntity<ErrorResponse> {

        var respuesta = ErrorResponse()
        var personas: List<Persona> = personaService.consultaPersonas() as List<Persona>
        var personasRes: List<PersonaResponse> = personas.stream().map { data -> PersonaResponse(data) }.collect(Collectors.toList())
        try {
            respuesta.responseError = ""
            respuesta.responseStatus = 200
            respuesta.result = personasRes
            return ResponseEntity(respuesta, HttpStatus.OK)
        } catch (e: CoreException) {
            respuesta.responseError = e.message
            respuesta.responseStatus = e.code
            respuesta.result = null
            return ResponseEntity(respuesta, HttpStatus.OK)
        }
    }
}
