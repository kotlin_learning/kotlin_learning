package com.local.kotlin.kotlinlegosoftlearning.dao.Impl

import com.local.kotlin.kotlinlegosoftlearning.dao.ITipoPersonaDao
import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Repository


@Repository
class TipoPersonaDaoImpl : ITipoPersonaDao {

    @Autowired
    var mongoTemplate: MongoTemplate? = null

    override fun consultaTipoPersonas(): MutableList<TipoPersona> {
        return mongoTemplate!!.findAll(TipoPersona::class.java);
    }
}
