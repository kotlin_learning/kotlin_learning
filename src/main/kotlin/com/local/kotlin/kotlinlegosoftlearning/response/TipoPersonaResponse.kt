package com.local.kotlin.kotlinlegosoftlearning.response

import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona

class TipoPersonaResponse {
    var id: Long = 0;
    var nombre: String = ""

    constructor(data: TipoPersona?) {
        this.id = data!!.id;
        this.nombre = data.nombre;
    }

}
