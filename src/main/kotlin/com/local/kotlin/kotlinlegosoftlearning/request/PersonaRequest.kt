package com.local.kotlin.kotlinlegosoftlearning.request

class PersonaRequest {
    var id = Math.floor(Math.random() * 101).toInt();
    var nombre: String = "";
    var apellidoPaterno: String = "";
    var apellidoMaterno: String = "";
    var curp: String = "";
    var rfc: String = "";
    lateinit var tipoPersona: TipoPersonaRequest
}
