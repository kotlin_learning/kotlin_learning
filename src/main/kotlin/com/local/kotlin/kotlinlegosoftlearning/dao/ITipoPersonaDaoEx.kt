package com.local.kotlin.kotlinlegosoftlearning.dao

import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona

interface ITipoPersonaDaoEx : IGenericDao<TipoPersona, Long>
