package com.local.kotlin.kotlinlegosoftlearning.dao

import java.io.Serializable

interface IGenericDao<T, ID : Serializable> {

    fun save(t: T): T;

    fun update(t: T): T;

    fun getById(id: ID): T;

    fun getAll(): MutableList<T>?;
}
