package com.local.kotlin.kotlinlegosoftlearning.controllers

import com.local.kotlin.kotlinlegosoftlearning.exception.CoreException
import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona
import com.local.kotlin.kotlinlegosoftlearning.response.ErrorResponse
import com.local.kotlin.kotlinlegosoftlearning.response.TipoPersonaResponse
import com.local.kotlin.kotlinlegosoftlearning.service.ITipoPersonaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.stream.Collectors
import javax.ws.rs.Consumes
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@CrossOrigin
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RestController
@RequestMapping("/persona/v1")
class TipoPersonaRestService {

    @Autowired
    lateinit var tipoPersonaService: ITipoPersonaService;

    @PostMapping(value = "tipoPersonas")
    fun consultaTiposPersonas(): ResponseEntity<ErrorResponse> {

        var respuesta = ErrorResponse()
        var lstTipoPersona: List<TipoPersona> = tipoPersonaService.consultaTiposPersonas() as List<TipoPersona>;
        var lstTipoPersonaRes: List<TipoPersonaResponse> = lstTipoPersona.stream().map { data -> TipoPersonaResponse(data) }.collect(Collectors.toList());
        try {
            respuesta.responseError = "";
            respuesta.responseStatus = 200;
            respuesta.result = lstTipoPersonaRes;
            return ResponseEntity(respuesta, HttpStatus.OK)
        } catch (e: CoreException) {
            respuesta.responseError = e.message;
            respuesta.responseStatus = e.code;
            respuesta.result = null;
            return ResponseEntity(respuesta, HttpStatus.OK)
        }

    }
}
