package com.local.kotlin.kotlinlegosoftlearning.config;

import com.mongodb.MongoClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import lombok.RequiredArgsConstructor;

/**
 * @author Marcos Barbero
 */
@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MultipleMongoProperties.class)
public class MultipleMongoConfig {

    private final MultipleMongoProperties mongoProperties;
    @Autowired
    private Environment env;

    @Primary
    @Bean(name = PrimaryMongoConfig.MONGO_TEMPLATE)
    public MongoTemplate primaryMongoTemplate() throws Exception {
        return new MongoTemplate(primaryFactory(this.mongoProperties.getPrimary()));
    }

//    @Bean(name = SecondaryMongoConfig.MONGO_TEMPLATE)
//    public MongoTemplate secondaryMongoTemplate() throws Exception {
//        return new MongoTemplate(secondaryFactory(this.mongoProperties.getSecondary()));
//    }

    @Bean
    @Primary
    public MongoDbFactory primaryFactory(final MongoProperties mongo) throws Exception {
//        return new SimpleMongoDbFactory(new MongoClient(mongo.getHost(), mongo.getPort()),
//                mongo.getDatabase());
        return new SimpleMongoDbFactory(new MongoClient(env.getProperty("spring.data.mongodb.host"), Integer.valueOf(env.getProperty("spring.data.mongodb.port"))),
                env.getProperty("spring.data.mongodb.database"));
    }

//    @Bean
//    public MongoDbFactory secondaryFactory(final MongoProperties mongo) throws Exception {
//        return new SimpleMongoDbFactory(new MongoClient(mongo.getHost(), mongo.getPort()),
//                mongo.getDatabase());
//    }

}
