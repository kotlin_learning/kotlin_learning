package com.local.kotlin.kotlinlegosoftlearning.dao.Impl

import com.local.kotlin.kotlinlegosoftlearning.dao.IGenericDao
import org.springframework.transaction.annotation.Transactional
import java.io.Serializable
import java.lang.reflect.ParameterizedType
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.Root
import javax.persistence.criteria.CriteriaBuilder


@Transactional
class GenericDaoImpl<T, ID : Serializable> : IGenericDao<T, ID> {

    var clasePersistente: Class<T>

    @PersistenceContext
    protected lateinit var entityManager: EntityManager

    constructor() {
        val genericSuperclass = javaClass.genericSuperclass as ParameterizedType
        this.clasePersistente = genericSuperclass.getActualTypeArguments()[0] as Class<T>
    }

    override fun save(t: T) = entityManager.merge(t);

    override fun update(t: T) = entityManager.merge(t);

    override fun getById(id: ID) = entityManager.find(clasePersistente, id);

    override fun getAll(): MutableList<T>? {
        val builder = entityManager.criteriaBuilder;
        val criteria = builder.createQuery(clasePersistente);
        val root = criteria.from(clasePersistente);
        criteria.select(root);
        return entityManager.createQuery(criteria).getResultList();
    }


}
