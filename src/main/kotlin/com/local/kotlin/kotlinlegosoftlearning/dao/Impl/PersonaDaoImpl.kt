package com.local.kotlin.kotlinlegosoftlearning.dao.Impl

import com.local.kotlin.kotlinlegosoftlearning.dao.IPersonaDao
import com.local.kotlin.kotlinlegosoftlearning.model.Persona
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.update
import org.springframework.stereotype.Repository

@Repository
class PersonaDaoImpl : IPersonaDao {

    @Autowired
    lateinit var mongoTemplate: MongoTemplate;

    override fun savePersona(persona: Persona): Persona {
        return mongoTemplate.save(persona)
    }

    override fun updatePersona(persona: Persona): Persona {
        return mongoTemplate.save(persona)
    }

    override fun consultaPersonas(): List<Persona> {
        return mongoTemplate.findAll(Persona::class.java)
    }
}
