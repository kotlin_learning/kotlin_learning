package com.local.kotlin.kotlinlegosoftlearning.utils

interface Constantes {

    companion object {
        val NO_REGISTER = "No se encontraron registros"
        val NO_ENTITY_FOUND = "No entity found";
        val NO_REGISTER_CODE = 1204L
        val CONFLIC = "No se guardo el registro"
        val CONFLIC_CODE = 1409L
        val DATA_INTEGRITY_VIOLATION = "DataIntegrityViolationException"
        val UNIQUE_KEY = "Violation of UNIQUE KEY"
        val UNIQUE_KEY_CODE = 1401L
    }

}