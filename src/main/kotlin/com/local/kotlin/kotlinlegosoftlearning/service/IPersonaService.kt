package com.local.kotlin.kotlinlegosoftlearning.service

import com.local.kotlin.kotlinlegosoftlearning.request.PersonaRequest

interface IPersonaService {

    fun creaPersona(request: PersonaRequest): Any?;

    fun consultaPersonas(): Any?;
}
