package com.local.kotlin.kotlinlegosoftlearning.service

import com.local.kotlin.kotlinlegosoftlearning.request.AplicativoRequest
import com.local.kotlin.kotlinlegosoftlearning.request.IdRequest

interface IAplicativoService {

    fun saveAplicativo(request: AplicativoRequest): Any?

    fun aplicativoById(request: IdRequest): Any?
}