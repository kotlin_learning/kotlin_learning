package com.local.kotlin.kotlinlegosoftlearning.utils

//fun main(args: Array<String>) {
//    forLoop()
////    whenLoop(2)
////    casteo()
//
//}


fun forLoop() {
    for (i in 1..100000) {
        println(i)
    }
}

fun whenLoop(x: Int) {
    when (x) {
        in 1..3 -> println("opcion 1")
        in 4..5 -> println("opcion 2")
    }
}

fun casteo() {
    var cad = '>'
    var num: Int

    num =  cad.toInt()
    println(num)
}
