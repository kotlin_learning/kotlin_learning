package com.local.kotlin.kotlinlegosoftlearning.response

import com.local.kotlin.kotlinlegosoftlearning.model.Persona

class PersonaResponse {
    var _id: Long = 0;
    var nombre: String = "";
    var apellidoPaterno: String = "";
    var apellidoMaterno: String = "";
    var curp: String = "";
    var rfc: String = "";
    var tipoPersona: TipoPersonaResponse? = null;

    constructor(persona: Persona) {
        this._id = persona._id
        this.nombre = persona.nombre;
        this.apellidoPaterno = persona.apellidoPaterno;
        this.apellidoMaterno = persona.apellidoMaterno;
        this.curp = persona.curp;
        this.rfc = persona.rfc;
        this.tipoPersona = TipoPersonaResponse(persona.tipoPersona)
    }
}
