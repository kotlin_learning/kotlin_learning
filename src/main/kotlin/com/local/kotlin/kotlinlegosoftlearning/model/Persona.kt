package com.local.kotlin.kotlinlegosoftlearning.model

import com.local.kotlin.kotlinlegosoftlearning.request.PersonaRequest
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import javax.persistence.Id

@Document(collection = "Persona")
data class Persona(var _id: Long) {
    @Indexed(unique = true)
    @Field(value = "id")
    var id: Long = 0
    internal var nombre: String = ""
    internal var apellidoPaterno: String = ""
    internal var apellidoMaterno: String = ""
    internal var curp: String = ""
    internal var rfc: String = ""
    internal var tipoPersona: TipoPersona? = null;

    constructor() : this(0)

    constructor(request: PersonaRequest) : this() {
        this._id = request.id.toLong()
        this.id = request.id.toLong()
        this.nombre = request.nombre;
        this.apellidoPaterno = request.apellidoPaterno;
        this.apellidoMaterno = request.apellidoMaterno;
        this.curp = request.curp;
        this.rfc = request.rfc;
        this.tipoPersona = TipoPersona(request.tipoPersona)
    }


}

