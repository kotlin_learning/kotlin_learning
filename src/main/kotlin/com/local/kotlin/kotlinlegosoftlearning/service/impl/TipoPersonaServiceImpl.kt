package com.local.kotlin.kotlinlegosoftlearning.service.impl

import com.google.common.base.Throwables
import com.local.kotlin.kotlinlegosoftlearning.dao.ITipoPersonaDao
import com.local.kotlin.kotlinlegosoftlearning.exception.CoreException
import com.local.kotlin.kotlinlegosoftlearning.model.TipoPersona
import com.local.kotlin.kotlinlegosoftlearning.service.ITipoPersonaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class TipoPersonaServiceImpl : ITipoPersonaService {

    @Autowired
    lateinit var tipoPersonaDao: ITipoPersonaDao;

    override fun consultaTiposPersonas(): MutableList<TipoPersona>? {
        try {
            return tipoPersonaDao.consultaTipoPersonas();
        } catch (e: Exception) {
            Throwables.getRootCause(e);
            throw CoreException(e);
        }
    }
}
