package com.local.kotlin.kotlinlegosoftlearning.exception

import com.google.common.base.Throwables
import com.local.kotlin.kotlinlegosoftlearning.utils.Constantes
import java.util.*

class CoreException(throwable: Throwable) : Exception(throwable) {


    override var message: String? = setMessage(throwable)
    var code: Long = setCode(message)


    fun setMessage(e: Throwable): String? {
        var rootCause = Throwables.getRootCause(e)
        if (Objects.isNull(rootCause)) return e.message else return rootCause.message
    }

    fun setCode(msg: String?): Long {

        when {
            msg!!.contains(Constantes.NO_REGISTER)
                    || msg!!.contains(Constantes.NO_ENTITY_FOUND) -> return Constantes.NO_REGISTER_CODE

            msg!!.contains(Constantes.UNIQUE_KEY) || msg in Constantes.DATA_INTEGRITY_VIOLATION -> return Constantes.UNIQUE_KEY_CODE
        }
        return 0
    }


}