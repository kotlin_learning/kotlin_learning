package com.local.kotlin.kotlinlegosoftlearning.dao

import com.local.kotlin.kotlinlegosoftlearning.model.Persona

interface IPersonaDao {

    fun savePersona(persona: Persona): Persona;

    fun updatePersona(persona: Persona): Persona;

    fun consultaPersonas(): List<Persona>;
}
